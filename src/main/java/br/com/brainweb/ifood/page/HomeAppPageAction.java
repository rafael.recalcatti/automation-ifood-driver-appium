package br.com.brainweb.ifood.page;

import br.com.brainweb.ifood.page.cadastro.CadastroEntregadorElementMap;
import br.com.brainweb.ifood.utils.SetUp;


public class HomeAppPageAction extends CadastroEntregadorElementMap {

    public void installAndOpenApp() {
        SetUp.instance();
    }

}
