package br.com.brainweb.ifood.page.cadastro;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CadastroEntregadorElementMap {

    @FindBy(id = "permissionAllowButton")
    protected WebElement btnAcceptLocation;

    @FindBy(id = "com.android.packageinstaller:id/permission_allow_button")
    protected WebElement btnAcceptLocationPermission;

    @FindBy(id = "signUpButton")
    protected WebElement btnSignUp;

    @FindBy(id = "signupNameTextInput")
    protected WebElement inputName;

    @FindBy(id = "signupDocumentTextInput")
    protected WebElement inputDocument;

    @FindBy(id = "signupPhoneTextInput")
    protected WebElement inputPhone;

    @FindBy(id = "signupEmailTextInput")
    protected WebElement inputEmail;

    @FindBy(id = "signupPolicyCheckBox")
    protected WebElement chboxSignupPolicy;

    @FindBy(id = "signupRegisterButton")
    protected WebElement btnConfirmRegister;

    @FindBy(id = "br.com.ifood.driver.app:id/textinput_error")
    protected WebElement txtInputError;



}
