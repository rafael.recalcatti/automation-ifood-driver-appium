package br.com.brainweb.ifood.page.cadastro;

import br.com.brainweb.ifood.utils.SetUp;
import br.com.brainweb.ifood.utils.Util;
import io.appium.java_client.MobileElement;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.UUID;

public class CadastroEntregadorPageAction extends CadastroEntregadorElementMap {

    private WebDriverWait wait = null;

    public CadastroEntregadorPageAction() {
        wait = new WebDriverWait(SetUp.instance().getDriver(), 10);
        PageFactory.initElements(SetUp.instance().getDriver(), this);
    }

    public void acceptLocation() {
        wait.until(ExpectedConditions.visibilityOf(btnAcceptLocation));
        btnAcceptLocation.click();
        btnAcceptLocationPermission.click();
    }

    public void clickRegisterDriver() {
        btnSignUp.click();
    }

    public void inputInformationDriver() {
        try {
            wait.until(ExpectedConditions.visibilityOf(inputName));
            inputName.sendKeys("TESTE");
            inputDocument.sendKeys("82272085090");
            inputPhone.sendKeys("51995554478");
            MobileElement mobileElement = SetUp.instance().getDriver().findElement(Util.getByFromElement(inputEmail));
            Util.setValue(mobileElement, "teste@gmail.com");
            inputDocument.click();
            wait.until(ExpectedConditions.visibilityOf(chboxSignupPolicy));
            chboxSignupPolicy.click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void confirmRegisterDriver() {
        btnConfirmRegister.click();
    }

    public boolean validNameInvalid(String msgExpected) {
        String msgError = txtInputError.getText();
        Util.screenShot();
        return msgError.equals(msgExpected);
    }
}
