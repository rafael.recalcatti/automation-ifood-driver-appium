package br.com.brainweb.ifood.utils;

import io.appium.java_client.MobileElement;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class Util {

    public static By getByFromElement(WebElement element) {

        By by = null;
        String[] selectorWithValue = (element.toString().split("->")[1].replaceFirst("(?s)(.*)\\]", "$1" + "")).split(":");

        String selector = selectorWithValue[0].trim();
        String value = selectorWithValue[1].trim();

        switch (selector) {
            case "id":
                by = By.id(value);
                break;
            case "className":
                by = By.className(value);
                break;
            case "tagName":
                by = By.tagName(value);
                break;
            case "xpath":
                by = By.xpath(value);
                break;
            case "cssSelector":
                by = By.cssSelector(value);
                break;
            case "linkText":
                by = By.linkText(value);
                break;
            case "name":
                by = By.name(value);
                break;
            case "partialLinkText":
                by = By.partialLinkText(value);
                break;
            default:
                throw new IllegalStateException("locator : " + selector + " not found!!!");
        }
        return by;
    }

    public static void setValue(MobileElement mobileElement, String s) {
        for (Character c : s.toCharArray()) {
            mobileElement.setValue(c.toString());
        }
    }

    public static void screenShot() {
        try {
            File scrFile = ((TakesScreenshot) SetUp.instance().getDriver()).getScreenshotAs(OutputType.FILE);
            String path = "target/screenshots/" + UUID.randomUUID() + "" + ".png";
            File screenshotLocation = null;
            screenshotLocation = new File(System.getProperty("user.dir") + "/" + path);
            FileUtils.copyFile(scrFile, screenshotLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
