package br.com.brainweb.ifood.utils;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.service.ExtentService;
import com.aventstack.extentreports.service.ExtentTestManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;


public class ListenerTest implements org.testng.ITestListener {

    protected static final Logger LOGGER = LogManager.getLogger();

    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        ExtentTestManager.log(iTestResult, true);
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        ExtentTestManager.log(iTestResult, true);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        ExtentService.getInstance().flush();
    }

    public void reportInfo(String log) {
        report(Status.INFO, log);
        LOGGER.info(log);
    }


    public void info(Object log) {
        report(Status.INFO, log.toString());
        LOGGER.info(log);
    }

    public void reportPass(String log) {
        report(Status.PASS, log);
        LOGGER.info(log);
    }

    public void reportFail(String log) {
        report(Status.FAIL, log);
        LOGGER.error(log);
    }

    private void report(Status status, String log) {
        ExtentTestManager.getTest().log(status, log);
    }

}