package br.com.brainweb.ifood.utils;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


public class SetUp {

    private static URL url;
    private static DesiredCapabilities capabilities;

    public static AndroidDriver<MobileElement> getDriver() {
        return driver;
    }

    private static AndroidDriver<MobileElement> driver;
    public static SetUp instance;

    private SetUp() {

    }

    public static SetUp instance() {

        if (instance == null) {
            try {
                instance = new SetUp();
                setupAppium();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        return instance;

    }

    private static void setupAppium() throws MalformedURLException {

        String path = "src/test/resources/br.com.ifood.driver.app_3070101.apk";
        path = new File(path).getAbsolutePath();
        final String URL_STRING = "http://127.0.0.1:4723/wd/hub";
        url = new URL(URL_STRING);
        capabilities = new DesiredCapabilities();
        //capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Xiaomi Redmi Note 7");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        //capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9");
        if (Objects.nonNull(path))
            capabilities.setCapability(MobileCapabilityType.APP, path);

        capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        capabilities.setCapability(MobileCapabilityType.SUPPORTS_JAVASCRIPT, true);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
        driver = new AndroidDriver<>(url, capabilities);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.resetApp();
    }

}