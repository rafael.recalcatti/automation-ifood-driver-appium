package br.com.brainweb.ifood.testcase;

import br.com.brainweb.ifood.page.HomeAppPageAction;
import br.com.brainweb.ifood.page.cadastro.CadastroEntregadorPageAction;
import br.com.brainweb.ifood.utils.SetUp;
import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({ExtentITestListenerClassAdapter.class})
public class TestCase {


    @AfterSuite
    public void uninstallApp() throws InterruptedException {
        SetUp.instance().getDriver().removeApp("br.com.ifood.driver.app");
    }


    @Test(enabled = true)
    public void myFirstTest() {
        new HomeAppPageAction().installAndOpenApp();
        CadastroEntregadorPageAction action = new CadastroEntregadorPageAction();
        action.acceptLocation();
        action.clickRegisterDriver();
        action.inputInformationDriver();
        action.confirmRegisterDriver();
        Assert.assertTrue(action.validNameInvalid("Nome inválido"), "");
    }

}